#-------------------------------------------------
#
# Project created by QtCreator 2018-10-22T02:50:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = freemegb
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS += -fms-extensions

CONFIG += c++11

INCLUDEPATH += include

SOURCES += \
    src/cpu.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/system.cpp \
    src/preferencesdialog.cpp \
    src/screendialog.cpp \
    src/emulationpreferencesdialog.cpp \
    src/rom.cpp \
    src/instructions.cpp \
    src/mmu.cpp \
    src/memory.cpp

HEADERS += \
    include/cpu.h \
    include/logtype.h \
    include/mainwindow.h \
    include/system.h \
    include/registers.h \
    preferencesdialog.h \
    include/preferencesdialog.h \
    include/instructions.h \
    include/screendialog.h \
    include/emulationpreferencesdialog.h \
    include/rom.h \
    include/mmu.h \
    include/memory.h

FORMS += \
        mainwindow.ui \
    preferencesdialog.ui \
    screendialog.ui \
    emulationpreferencesdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/resources.qrc

win32: RC_ICONS = resources/icons/freemegb.ico

DISTFILES += \
    resources/icons/freemegb64.png \
    resources/icons/freemegb128.png \
    resources/icons/freemegb512.png \
    resources/icons/freemegb.ico \
    README.md
