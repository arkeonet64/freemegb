#ifndef CPU_H
#define CPU_H
#include "registers.h"
#include "instructions.h"

class CPU
{
public:
    CPU();
    void step();
    Registers registers;
    Instructions instructions;
};

#endif // CPU_H
