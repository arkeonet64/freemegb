#ifndef ROM_H
#define ROM_H

#include <QByteArray>

class ROM : public QByteArray
{
public:
    ROM();

    ROM operator=(const QByteArray &);
};

#endif // ROM_H
