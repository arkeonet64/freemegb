#include "system.h"
#include <QTimer>
#include <QEventLoop>
#include <QThread>
#include <QDebug>
#include "globals.h"

ROM System::rom;

System::System(QObject *parent) : QObject(parent)
{

}

System::~System()
{
}

void System::updateAllRegisters()
{
	if (DEBUG)
	{
		emit updateRegisterAF(QString("0x%1").arg(cpu.registers.af, 4, 16, QChar('0')).toUpper());
		emit updateRegisterBC(QString("0x%1").arg(cpu.registers.bc, 4, 16, QChar('0')).toUpper());
		emit updateRegisterDE(QString("0x%1").arg(cpu.registers.de, 4, 16, QChar('0')).toUpper());
		emit updateRegisterHL(QString("0x%1").arg(cpu.registers.hl, 4, 16, QChar('0')).toUpper());
		emit updateRegisterSP(QString("0x%1").arg(cpu.registers.sp, 4, 16, QChar('0')).toUpper());
		emit updateRegisterPC(QString("0x%1").arg(cpu.registers.pc, 4, 16, QChar('0')).toUpper());
	}
}

void System::init()
{
    cpu.registers.pc = 0x0100;
    cpu.registers.sp = 0xFFFE;
    cpu.registers.af = 0x01B0;
    cpu.registers.bc = 0x0013;
    cpu.registers.de = 0x00D8;
    cpu.registers.hl = 0x014D;

    updateAllRegisters();
}

void System::reset()
{
    init();
}

void System::stop()
{

}

void System::resume()
{

}
bool System::loadRom(QString romFile)
{
    log(QString("Loading Rom %1").arg(romFile), LogType::info);
    QFile _romFile(romFile);
    if (!_romFile.open(QIODevice::ReadOnly)) {
        log(QString("Rom %1 not loaded. An error occured").arg(romFile), LogType::error);
        _romFile.close();
        return false;
    } else {
        System::rom.clear();
        System::rom.append(_romFile.readAll());
        if (System::rom.length() > 0) {
            log(QString("Rom %1 loaded").arg(romFile), LogType::success);
            _romFile.close();
            return true;
        } else {
            log(QString("Rom %1 not loaded. An error occured").arg(romFile), LogType::error);
            _romFile.close();
            return false;
        }
    }
}
void System::start() {
    reset();


    while(true) {
        log(cpu.registers.toString(),
            LogType::info);
        cpu.step();

        thread()->msleep(500);
        updateAllRegisters();

        log(cpu.registers.toString(),
            LogType::info);
    }
}

void System::log(QString string, LogType logtype) {
    QString format = QString("<span style=\" font-size:8pt; font-weight:600; color:%1;\">[%2]:</span> %3");
    switch (logtype) {
    case info:
        string = format.arg("#FFFFFF", "INFO", string);
        break;
    case error:
        string = format.arg("#FF0000", "ERROR", string);
        break;
    case success:
        string = format.arg("#00FF00", "SUCCESS", string);
        break;
    case warning:
        string = format.arg("#FFFF00", "WARNING", string);
        break;
    }
    emit updateLog(string);
}
