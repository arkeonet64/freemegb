#ifndef SYSTEM_H
#define SYSTEM_H

#include "rom.h"
#include "cpu.h"
#include "logtype.h"
#include <QObject>
#include <QMutex>
#include <QWaitCondition>
#include <QString>
#include <QFile>

class System : public QObject
{
    Q_OBJECT
public:
    explicit System(QObject *parent = nullptr);
    ~System();
    void log(QString string, LogType logtype);
    void updateAllRegisters();
    void reset();
    void init();
    static ROM rom;

private:
    CPU cpu;
    QMutex lock;
    QWaitCondition waitCondition;

signals:
    void updateLog(QString string);
    void updateRegisterAF(QString);
    void updateRegisterBC(QString);
    void updateRegisterDE(QString);
    void updateRegisterHL(QString);
    void updateRegisterSP(QString);
    void updateRegisterPC(QString);

public slots:
    bool loadRom(QString romFile);
    void start();
    void stop();
    void resume();
};

#endif // SYSTEM_H
