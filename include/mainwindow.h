#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "system.h"
#include "preferencesdialog.h"
#include "emulationpreferencesdialog.h"
#include "screendialog.h"
#include <QtGui/qstandarditemmodel.h>
#include <QThread>
#include <QSettings>
#include <QMessageBox>
#include <QString>
#include <QList>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *evnt);

private slots:
    void on_actionQuit_triggered();
    void on_actionRun_triggered();
    void on_actionOpen_triggered();
    void on_actionFullscreen_triggered();
    void on_actionPreferences_triggered();
    void on_actionEmulationSettings_triggered();
    void on_actionInputSettings_triggered();
    void openRom();

    void on_actionDebug_triggered();

private:
    const int MAX_RECENT_FILES = 5;
    void shutdownSystem();
    void saveSettings();
    void loadSettings();
    QList<QString> recentFiles;
    QMessageBox::StandardButton displayQuitCheck();
    PreferencesDialog preferencesDialog;
    EmulationPreferencesDialog emulationPreferences;
    Ui::MainWindow *ui;
    QThread *systemThread;
    System *system;
    QString settingsFile;
	QStandardItemModel *tableModel;
    ScreenDialog screen;
};

#endif // MAINWINDOW_H
