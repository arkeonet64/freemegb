#include "memory.h"

const quint16 Memory::MEM_ROM_LIMIT; // [0x0000, 0x4000]
const quint16 Memory::MEM_8KB_RAM_MIN; // [0xC000, 0xE000)
const quint16 Memory::MEM_8KB_RAM_MAX; // [0xC000, 0xE000)
const quint16 Memory::MEM_MIRROR_RAM_MIN; // [0xE000, 0xFE00)
const quint16 Memory::MEM_MIRROR_RAM_MAX; // [0xE000, 0xFE00)
const quint16 Memory::MEM_MIRROR_OFFSET; // 8KB range offset
const quint16 Memory::MEM_SPRITE_RAM_MIN; // [0xFE00, 0xFEA0)
const quint16 Memory::MEM_SPRITE_RAM_MAX; // [0xFE00, 0xFEA0)
const quint16 Memory::MEM_HI_RAM_MIN; // [0xFF80, 0xFFFF)
const quint16 Memory::MEM_HI_RAM_MAX; // [0xFF80, 0xFFFF)
const quint16 Memory::MEM_IO_RAM_MIN; // [0xFF00, 0xFF4B)
const quint16 Memory::MEM_IO_RAM_MAX; // [0xFF80, 0xFF4B)
const quint16 Memory::MEM_VIDEO_RAM_MIN; // [0x8000, 0xA000)
const quint16 Memory::MEM_VIDEO_RAM_MAX; // [0x8000, 0xA000)

Memory::Memory()
{

}
