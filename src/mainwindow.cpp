#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <Qt>
#include <QThread>
#include <QEventLoop>
#include <QTimer>
#include <QFileDialog>
#include <QCloseEvent>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include "globals.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	QCoreApplication::setApplicationName("FreeMe!GB");
	QCoreApplication::setOrganizationName("Nathaniel Martin");
	QCoreApplication::setOrganizationDomain("ioncloud64.com");
	qRegisterMetaTypeStreamOperators<QList<QString> >("QList<QString>");

	screen.setWindowFlags(screen.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	preferencesDialog.setWindowFlags(preferencesDialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	emulationPreferences.setWindowFlags(emulationPreferences.windowFlags() & ~Qt::WindowContextHelpButtonHint);

	ui->setupUi(this);

	tableModel = new QStandardItemModel(3, 4, this);
	tableModel->setHorizontalHeaderLabels(QStringList() << "Opcode" << "Instruction" << "Operand0" << "Operand1");
	tableModel->setVerticalHeaderLabels(QStringList() << "0x00" << "0x01" << "0x02");
	ui->tableView->setModel(tableModel);
	tableModel->setData(tableModel->index(0, 0), QVariant(QBrush(QColor(255, 0, 0, 128))), Qt::BackgroundColorRole);
	tableModel->setData(tableModel->index(0, 1), QVariant(QBrush(QColor(255, 0, 0, 128))), Qt::BackgroundColorRole);
	tableModel->setData(tableModel->index(0, 2), QVariant(QBrush(QColor(255, 0, 0, 128))), Qt::BackgroundColorRole);
	tableModel->setData(tableModel->index(0, 3), QVariant(QBrush(QColor(255, 0, 0, 128))), Qt::BackgroundColorRole);

	settingsFile = QApplication::applicationDirPath() + "/settings.ini";
	systemThread = new QThread;
	systemThread->setObjectName("FreeMe!GB System");
	system = new System;
	system->moveToThread(systemThread);
	connect(system, SIGNAL(updateLog(QString)), ui->log, SLOT(append(QString)));
	connect(system, SIGNAL(updateRegisterAF(QString)), ui->labelRegisterAF, SLOT(setText(QString)));
	connect(system, SIGNAL(updateRegisterBC(QString)), ui->labelRegisterBC, SLOT(setText(QString)));
	connect(system, SIGNAL(updateRegisterDE(QString)), ui->labelRegisterDE, SLOT(setText(QString)));
	connect(system, SIGNAL(updateRegisterHL(QString)), ui->labelRegisterHL, SLOT(setText(QString)));
	connect(system, SIGNAL(updateRegisterSP(QString)), ui->labelRegisterSP, SLOT(setText(QString)));
	connect(system, SIGNAL(updateRegisterPC(QString)), ui->labelRegisterPC, SLOT(setText(QString)));
	connect(systemThread, SIGNAL(started()), system, SLOT(start()));

	loadSettings();
	DEBUG = ui->actionDebug->isChecked();
}

MainWindow::~MainWindow()
{
	delete system;
	delete systemThread;
	delete ui;
	delete tableModel;
}

void MainWindow::on_actionQuit_triggered()
{
	QMessageBox::StandardButton returnButton = displayQuitCheck();

	if (returnButton != QMessageBox::Yes) {
	}
	else {
		shutdownSystem();
		QApplication::quit();
	}
}

void MainWindow::on_actionRun_triggered()
{
	systemThread->start();
	screen.show();
}

void MainWindow::on_actionOpen_triggered()
{


	QString fileName = QFileDialog::getOpenFileName(this, tr("Open ROM"), "", tr("GameBoy ROM (*.gb)"));
	if (fileName != "") {
		if (emit system->loadRom(fileName)) {
			ui->actionRun->setEnabled(true);
			//if the number of files is less than five, add another menu entry, and update ui
			if (recentFiles.length() < MAX_RECENT_FILES && !recentFiles.contains(fileName)) {
				recentFiles.append(fileName);
				QFontMetrics metrics(ui->menuOpen_Recent->font());

				int width = ui->menuOpen_Recent->width() + 100;

				QString clippedText = metrics.elidedText(QString::number(recentFiles.length()) + ": " + fileName,
					Qt::ElideMiddle, width);
				ui->menuOpen_Recent->addAction(clippedText);
				if (!ui->menuOpen_Recent->isEnabled()) {
					ui->menuOpen_Recent->setEnabled(true);
				}
			}
			else if (recentFiles.length() == MAX_RECENT_FILES && !recentFiles.contains(fileName)) {
				QList<QAction *> actions = ui->menuOpen_Recent->actions();
				foreach(QAction *menuAction, actions) { //clear all actions
					ui->menuOpen_Recent->removeAction(menuAction);
				}
				recentFiles.removeAt(0);
				recentFiles.append(fileName);
				for (int i = 0; i < recentFiles.length(); i++) {
					QFontMetrics metrics(ui->menuOpen_Recent->font());

					int width = ui->menuOpen_Recent->width() + 100;

					QString clippedText = metrics.elidedText(QString::number((i + 1)) + ": " + recentFiles[i],
						Qt::ElideMiddle, width);
					QAction *action = ui->menuOpen_Recent->addAction(clippedText);
					connect(action, SIGNAL(triggered()), this, SLOT(openRom()));
				}
			}
			saveSettings();
		}
	}
}

QMessageBox::StandardButton MainWindow::displayQuitCheck()
{
	QMessageBox::StandardButton returnButton =
		QMessageBox::question(this, QCoreApplication::applicationName(),
			tr("Are you sure?\n"),
			QMessageBox::No | QMessageBox::Yes,
			QMessageBox::Yes);

	return returnButton;
}

void MainWindow::shutdownSystem() {
	if (systemThread->isRunning()) {
		system->log("Terminating GameBoy System", LogType::info);
		systemThread->terminate();
		systemThread->wait();
	}
	if (systemThread->isFinished()) {
		system->log("Termination successful", LogType::success);
	}
	else {
		system->log("Termination unsuccessful; an unknown error occured", LogType::error);
	}
}

void MainWindow::saveSettings()
{
	QSettings settings(settingsFile, QSettings::IniFormat);
	settings.beginGroup("MainWindow");
	settings.setValue("fullscreen", ui->actionFullscreen->isChecked());
	settings.setValue("debug", ui->actionDebug->isChecked());
	settings.setValue("recentFiles", QVariant::fromValue(recentFiles));
	settings.endGroup();
}

void MainWindow::loadSettings()
{
	QSettings settings(settingsFile, QSettings::IniFormat);
	settings.beginGroup("MainWindow");
	ui->actionFullscreen->setChecked(settings.value("fullscreen").toBool());
	ui->actionDebug->setChecked(settings.value("debug").toBool());
	recentFiles.clear();

	recentFiles = settings.value("recentFiles").value<QList<QString>>();

	settings.endGroup();

	if (recentFiles.length() > 0) {
		for (int i = 0; i < recentFiles.length(); i++) {
			QFontMetrics metrics(ui->menuOpen_Recent->font());

			int width = ui->menuOpen_Recent->width() + 100;

			QString clippedText = metrics.elidedText(QString::number((i + 1)) + ": " + recentFiles[i],
				Qt::ElideMiddle, width);

			QAction *action = ui->menuOpen_Recent->addAction(clippedText);

			connect(action, SIGNAL(triggered()), this, SLOT(openRom()));
		}
		ui->menuOpen_Recent->setEnabled(true);
	}
}

void MainWindow::closeEvent(QCloseEvent *evnt)
{
	QMessageBox::StandardButton returnButton = displayQuitCheck();

	if (returnButton != QMessageBox::Yes) {
		evnt->ignore();
	}
	else {
		shutdownSystem();
		if (screen.isVisible()) screen.close();
		evnt->accept();
	}
}

void MainWindow::on_actionFullscreen_triggered()
{
	if (ui->actionFullscreen->isChecked()) {

	}
	else {

	}

	saveSettings();
}

void MainWindow::on_actionPreferences_triggered()
{
	preferencesDialog.show();
}

void MainWindow::on_actionEmulationSettings_triggered()
{
	emulationPreferences.show();
}

void MainWindow::on_actionInputSettings_triggered()
{
	emulationPreferences.show();
}

void MainWindow::openRom()
{
	QAction *action = qobject_cast<QAction*>(sender());
	QFuture<bool> future = QtConcurrent::run(system, &System::loadRom, recentFiles[QString(action->text().at(0)).toInt() - 1]);
	if (future.result()) {
		ui->actionRun->setEnabled(true);
	}
}

void MainWindow::on_actionDebug_triggered()
{
	DEBUG = ui->actionDebug->isChecked();
	saveSettings();
	}
