#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <QDebug>
#include "registers.h"

class Instructions;
typedef void (Instructions::*instruction)();


class Instructions
{
public:
    Instructions(Registers &);

    instruction &operator[](int index)
    {
        return instructions[index];
    }

private:
    bool firstOperandCalled;
    instruction instructions[0x100];

    Registers *registers;

    // Instructions
    /**
     * @brief No operation instruction
     */
    void nop();

    void jp_nn();
    void jp_nz();

    /**
     * @brief Instruction @0x80
     */
    void add_a_b();
    void add_a_c();
    void add_a_d();
    void add_a_e();
    void add_a_h();
    void add_a_l();
    void add_a_hl();
    void add_a_n();
    void add_a_a();

    // Helper functions
    void add(register8 &, register8);
    void add(register8 &, register16);
    void add(register16 &, register16);

    void subtract(register8 &, register8);
    void subtract(register8 &, register16);
    void subtract(register16 &, register16);

    void increment(register8 &);
    void increment(register16 &);

    void decrement(register8 &);
    void decrement(register16 &);

    void load(register8 &);
    void load(register16 &);

	template <typename RegSize>
    /**
     * @brief getOperand0 - gets the first operand and auto-increments the PC Register to its position
     * firstOperandCalled gets set to true (this is to prevent possible incursion of an inexperienced/malicious programmer from calling operands out of order).
     * @return The next operand value as a byte (quint8) NULL if called out of order
     */
    RegSize getOperand0(RegSize rSize);

	template <typename RegSize>
    /**
     * @brief getOperand1 - gets the second operand and auto-increments the PC Register to its position
     * firstOperandCalled gets reset to false (this is to prevent possible incursion of an inexperienced/malicious programmer from calling operands out of order).
     * @return The next operand value as a byte (quint8); NULL if called out of order
     */
    RegSize getOperand1(RegSize rSize);

	register16 combine(register8, register8);

    bool isFlagSet(FLAG);
    void clearFlag(FLAG);
    void setFlag(FLAG);

    void _and(register8);
    void _or(register8);
    void _xor(register8);

    void notImplementedInstruction();
    void resetOperandFlag();
};

#endif // INSTRUCTIONS_H
