#ifndef EMULATIONPREFERENCESDIALOG_H
#define EMULATIONPREFERENCESDIALOG_H

#include <QDialog>

namespace Ui {
class EmulationPreferencesDialog;
}

class EmulationPreferencesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EmulationPreferencesDialog(QWidget *parent = nullptr);
    ~EmulationPreferencesDialog();

private:
    Ui::EmulationPreferencesDialog *ui;
};

#endif // EMULATIONPREFERENCESDIALOG_H
