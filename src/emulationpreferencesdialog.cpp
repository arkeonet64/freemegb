#include "emulationpreferencesdialog.h"
#include "ui_emulationpreferencesdialog.h"

EmulationPreferencesDialog::EmulationPreferencesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EmulationPreferencesDialog)
{
    ui->setupUi(this);
}

EmulationPreferencesDialog::~EmulationPreferencesDialog()
{
    delete ui;
}
