# FreeMe!GB
## Technical Description
FreeMe!GB is a project written in C++ with Qt GUI libraries. In order to work on this project, you will need to use Qt Creator 4.7.1 and MinGW 5.3.0 32-bit or similar build environment for Linux/Mac OS X.

## About
FreeMe!GB is an emulator for the original GameBoy created by Nintendo. The projects main goal is to provide accurate system emulation based on the architecture present in the GameBoy system.

NOTE: This project is for research only, and is not endorsed, affiliated, or attempting to infringe on copyrights placed by Nintendo.