#ifndef LOGTYPE_H
#define LOGTYPE_H

enum LogType {
    info,
    error,
    warning,
    success
};

#endif // LOGTYPE_H
