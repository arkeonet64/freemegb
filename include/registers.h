#ifndef REGISTERS_H
#define REGISTERS_H
#include <QtGlobal>
#include <QString>

typedef quint8 register8;
typedef quint16 register16;

enum FLAG {
    ZERO        = 1 << 7,
    SUBTRACT    = 1 << 6,
    HALF_CARRY  = 1 << 5,
    CARRY       = 1 << 4
};

struct Registers {
    struct {
        union {
            struct {
                register8 f;
                register8 a;
            };
            register16 af;
        };
    };
    struct {
        union {
            struct {
                register8 b;
                register8 c;
            };
            register16 bc;
        };
    };
    struct {
        union {
            struct {
                register8 e;
                register8 d;
            };
            register16 de;
        };
    };
    struct {
        union {
            struct {
                register8 l;
                register8 h;
            };
            register16 hl;
        };
    };

    register16 sp;
    register16 pc;

    QString toString()
    {
        return QString("CPU Registers: { AF[%1], BC[%2], DE[%3], HL[%4], SP[%5], PC[%6] }").arg(
            QString("0x%1").arg(af, 4, 16, QChar('0')).toUpper(),
            QString("0x%1").arg(bc, 4, 16, QChar('0')).toUpper(),
            QString("0x%1").arg(de, 4, 16, QChar('0')).toUpper(),
            QString("0x%1").arg(hl, 4, 16, QChar('0')).toUpper(),
            QString("0x%1").arg(sp, 4, 16, QChar('0')).toUpper(),
            QString("0x%1").arg(pc, 4, 16, QChar('0')).toUpper());
    }
};


#endif // REGISTERS_H
