#ifndef MEMORY_H
#define MEMORY_H

#include <QtGlobal>
#include "registers.h"

class Memory
{
public:
    Memory();

    static quint8 ROM[0x400000];


    static const quint16 MEM_ROM_LIMIT     = 0x4000; // [0x0000, 0x4000]
    static const quint16 MEM_8KB_RAM_MIN   = 0xC000; // [0xC000, 0xE000)
    static const quint16 MEM_8KB_RAM_MAX   = 0xE000; // [0xC000, 0xE000)
    static const quint16 MEM_MIRROR_RAM_MIN= 0xE000; // [0xE000, 0xFE00)
    static const quint16 MEM_MIRROR_RAM_MAX= 0XFE00; // [0xE000, 0xFE00)
    static const quint16 MEM_MIRROR_OFFSET = 0x2000; // 8KB range offset
    static const quint16 MEM_SPRITE_RAM_MIN= 0xFE00; // [0xFE00, 0xFEA0)
    static const quint16 MEM_SPRITE_RAM_MAX= 0XFEA0; // [0xFE00, 0xFEA0)
    static const quint16 MEM_HI_RAM_MIN    = 0xFF80; // [0xFF80, 0xFFFF)
    static const quint16 MEM_HI_RAM_MAX    = 0XFFFF; // [0xFF80, 0xFFFF)
    static const quint16 MEM_IO_RAM_MIN    = 0xFF00; // [0xFF00, 0xFF4B)
    static const quint16 MEM_IO_RAM_MAX    = 0XFF4B; // [0xFF80, 0xFF4B)
    static const quint16 MEM_VIDEO_RAM_MIN = 0x8000; // [0x8000, 0xA000)
    static const quint16 MEM_VIDEO_RAM_MAX = 0xA000; // [0x8000, 0xA000)
};

#endif // MEMORY_H
