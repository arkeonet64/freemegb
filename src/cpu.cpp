#include "cpu.h"
#include "system.h"

CPU::CPU()
    : instructions(registers)
{
}



void CPU::step()
{
    quint8 instruction_code = static_cast<quint8>(System::rom[registers.pc]); //gather instruction code

// Can't decide if I prefer this better, or the method below. For now I will use the below method to call instruction functions
//
//    instruction ins = instructions[instruction_code];
//    (instructions.*ins)();
	(instructions.*instructions[instruction_code])(); //instruction will handle operations of the registers
    registers.pc++;
}
