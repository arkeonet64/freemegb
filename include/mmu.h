#ifndef MMU_H
#define MMU_H

#include "memory.h"

class MMU
{
public:
    MMU();

    static quint8 parseMemoryLocation(register16);
};

#endif // MMU_H
